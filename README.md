# CreateTokensByResponse

Allow creation of tokens with value insixde the current survey on other surveys.

## Usage

On tools menu : select the surveys where token can be created, select the attribute to set and the values to set. Values can came from surveys response with expression manager. Each values are separated by , by default.

**Warning** This plugin is not compatible with token attribute encrypt


## Home page & Copyright
- HomePage <https://sondages.pro/>
- Copyright © 2023 Denis Chenu <https://sondages.pro>
- Copyright © 2023 OECD <https://oecd.org>
- [Support](https://support.sondages.pro)
- [Donate](https://support.sondages.pro/open.php?topicId=12)
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)

Distributed under [GNU AFFERO GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/agpl.txt) licence

