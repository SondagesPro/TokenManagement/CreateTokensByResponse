<?php

/**
 * CreateTokensByResponse
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>

 * @license AGPL v3
 * @version 0.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 */

class CreateTokensByResponse extends PluginBase
{
    protected $storage = "DbStorage";

    protected static $description = "Create tokens in other survey after submission";
    protected static $name = "CreateTokensByResponse";

    /* @inheritdoc , no allowed public methods */
    public $allowedPublicMethods = [
        'actionSettings'
    ];

    public function init()
    {
        $this->subscribe("afterSurveyComplete");
        $this->subscribe("beforeSurveySettings");
        $this->subscribe("beforeToolsMenuRender");
    }
    public function afterSurveyComplete()
    {
        $completeEvent = $this->getEvent();
        $surveyId = $completeEvent->get("surveyId");
        $aResponse = $this->pluginManager
            ->getAPI()
            ->getResponse($surveyId, $completeEvent->get("responseId"));
        $aReplace = array(
            'SID' => $surveyId,
            'SAVEDID' => "0",
        );
        if (!empty($aResponse['id'])) {
            $aReplace['SAVEDID'] = $aResponse['id'];
        }
        /* get related surveys */
        $otherSurveys = $this->get('otherSurveys', 'Survey', $surveyId, []);
        if (empty($otherSurveys)) {
            return;
        }
        $attributeToSet = $this->get('attributeToUse', 'Survey', $surveyId, "");
        if (empty($attributeToSet)) {
            return;
        }
        $valuesToSet = $this->get('valuesToSet', 'Survey', $surveyId, "");
        $valuesSeparator = $this->get('valuesSeparator', 'Survey', $surveyId, "");
        $valuesToSet = trim(LimeExpressionManager::ProcessStepString($valuesToSet, $aReplace, 3, true));
        if (empty($valuesToSet)) {
            return;
        }
        $aValuesToSet = explode($valuesSeparator, $valuesToSet);
        $aValuesToSet = array_unique(array_filter(array_map('trim', $aValuesToSet)));
        if (empty($aValuesToSet)) {
            return;
        }
        $otherAttributes = array();
        $aTokenAttributes = $this->ownGetTokensAttributes($surveyId);
        if (!empty($aTokenAttributes)) {
            foreach ($aTokenAttributes as $attribute => $description) {
                $otherValue = trim($this->get("other_attributes_{$attribute}", "Survey", $surveyId, ""));
                if ($otherValue !== "") {
                    $otherValue = trim(LimeExpressionManager::ProcessStepString($otherValue, $aReplace, 3, true));
                }
                if ($otherValue !== "") {
                    $otherAttributes[$attribute] = $otherValue;
                }
            }
        }

        /* OK : lets'go */
        $attributesDuplicateExclude = (array) $this->get('attributesDuplicateExclude', 'Survey', $surveyId, array());
        foreach ($otherSurveys as $otherSurveyId) {
            if (!tableExists("{{tokens_{$otherSurveyId}}}")) {
                continue;
            }
            $aValidAttributes = Token::model($otherSurveyId)->getAttributes();
            if (!array_key_exists($attributeToSet, $aValidAttributes)) {
                continue;
            }
            foreach ($aValuesToSet as $valueToSet) {
                /* Search the token */
                $tokenCriteria = new CDbCriteria();
                $tokenCriteria->compare(App()->db->quoteColumnName($attributeToSet), $valueToSet);
                foreach ($otherAttributes as $attribute => $value) {
                    if (array_key_exists($attribute, $aValidAttributes) && !in_array($attribute, $attributesDuplicateExclude)) {
                        $tokenCriteria->compare(App()->db->quoteColumnName($attribute), $value);
                    }
                }
                $oToken = Token::model($otherSurveyId)->find($tokenCriteria);
                if (empty($oToken)) {
                    $oToken = Token::create($otherSurveyId);
                    $oToken->setAttribute($attributeToSet, $valueToSet);
                    $oToken->generateToken();
                } else {
                }
                foreach ($otherAttributes as $attribute => $value) {
                    if (array_key_exists($attribute, $aValidAttributes)) {
                        $oToken->setAttribute($attribute, $value);
                    }
                }
                if (!$oToken->save()) {
                    tracevar($oToken->getErrors());
                    Yii::log(
                        "Error when create token for survey $otherSurveyId width $valueToSet",
                        "error",
                        "plugin.createTokensByResponse.afterSurveyComplete"
                    );
                    Yii::log(
                        CVarDumper::dumpAsString($oToken->getErrors()),
                        "warning",
                        "plugin.createTokensByResponse.afterSurveyComplete"
                    );
                }
            }
        }
    }

    public function newSurveySettings()
    {
        /* save it directly : maybe use array for setting token_ ? */
        foreach ($this->event->get("settings") as $name => $value) {
            $this->set(
                $name,
                $value,
                "Survey",
                $this->event->get("survey"),
                ""
            );
        }
    }
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        $oEvent->set("surveysettings.{$this->id}", [
            "name" => get_class($this),
            "settings" => [
                "gotosettings" => [
                    "type" => "link",
                    "label" => $this->translate(
                        "Update Create Tokens by Response settings"
                    ),
                    "htmlOptions" => [
                        "title" => $this->translate("This open another page"),
                    ],
                    "help" => $this->translate(
                        "This open a new page remind to save your settings."
                    ),
                    "text" => $this->translate(
                        "Edit Create Tokens by response settings"
                    ),
                    "class" => "btn btn-link",
                    "link" => Yii::app()->createUrl("admin/pluginhelper", [
                        "sa" => "sidebody",
                        "plugin" => get_class($this),
                        "method" => "actionSettings",
                        "surveyId" => $oEvent->get("survey"),
                    ]),
                ],
            ],
        ]);
    }
    /**
     * see beforeToolsMenuRender event
     *
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        $event = $this->getEvent();
        $surveyId = $event->get("surveyId");
        if (
            Permission::model()->hasSurveyPermission(
                $surveyId,
                "token",
                "create"
            )
        ) {
            $href = Yii::app()->createUrl("admin/pluginhelper", [
                "sa" => "sidebody",
                "plugin" => get_class($this),
                "method" => "actionSettings",
                "surveyId" => $surveyId,
            ]);
            $aMenuItem = [
                "label" => $this->translate("Token create"),
                "iconClass" => "fa fa-users", //'fa fa-edit',
                "href" => $href,
            ];
            $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
            $event->append("menuItems", [$menuItem]);
        }
    }

    /**
     * Main function
     * @param int $surveyId Survey id
     *
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(
                404,
                $this->translate("This survey does not seem to exist.")
            );
        }
        if (
            !Permission::model()->hasSurveyPermission(
                $surveyId,
                "token",
                "create"
            )
        ) {
            throw new CHttpException(401);
        }
        if (
            App()->getRequest()->getPost("save" . get_class($this))
        ) {
            $otherSurveyIds = (array) App()->getRequest()->getPost('otherSurveys');
            $otherSurveyIds = array_filter($otherSurveyIds);
            $validOtherSurveyId = array();
            if (!empty($otherSurveyIds)) {
                foreach ($otherSurveyIds as $otherSurveyId) {
                    if (Permission::model()->hasSurveyPermission($otherSurveyId, 'tokens', 'update')) {
                        $validOtherSurveyId[] = $otherSurveyId;
                    }
                }
            }
            $this->set('otherSurveys', $validOtherSurveyId, "Survey", $surveyId);
            $this->set('attributeToUse', App()->getRequest()->getPost('attributeToUse'), "Survey", $surveyId);
            $this->set('valuesToSet', App()->getRequest()->getPost('valuesToSet'), "Survey", $surveyId);
            $this->set('valuesSeparator', App()->getRequest()->getPost('valuesSeparator'), "Survey", $surveyId);

            $aTokenAttributes = $this->ownGetTokensAttributes($surveyId);
            foreach ($aTokenAttributes as $attribute => $label) {
                $setting = "other_attributes_" . $attribute;
                if (
                    !is_null(
                        App()->getRequest()->getPost($setting)
                    )
                ) {
                    $this->set($setting, App()->getRequest()->getPost($setting), "Survey", $surveyId);
                }
            }
            $this->set('attributesDuplicateExclude', App()->getRequest()->getPost('attributesDuplicateExclude', array()), "Survey", $surveyId);
            if (
                App()
                    ->getRequest()
                    ->getPost("save" . get_class($this) == "redirect")
            ) {
                Yii::app()
                    ->getController()
                    ->redirect(
                        Yii::app()->getController()->createUrl("admin/survey", [
                                "sa" => "view",
                                "surveyid" => $surveyId,
                            ])
                    );
            }
        }

        $aSettings = array();
        /* Basic settings (on this survey) */
        $aSurveyListData = $this->ownGetSurveyListData();
        unset($aSurveyListData[$surveyId]);
        /* attribute are updated with list of related survey + this survey */
        $aTokenAttributes = $this->ownGetTokensAttributes($surveyId);
        $valuesToBeSethelp = "";
        if (intval(App()->getConfig('versionnumber')) > 3) {
            $aSettings[$this->translate('Warning')] = array(
                "warningCompapt" => [
                    'type' => 'info',
                    'content' => "<div class='alert alert-danger'>" . $this->translate("This plugin is not compatible with token attributes encryption. If you use encryption on token attributes : this can break your token table.") . "</div>"
                ],
            );
        }
        $aSettings[$this->translate('Create tokens settings')] = array(
            "otherSurveys" => [
                'type' => 'select',
                'label' => $this->translate('Surveys for tokens creation.'),
                'help' => $this->translate('Tokens was created or updated according to other settings and this list for existing token.'),
                'options' => $aSurveyListData,
                'htmlOptions' => [
                    'multiple' => true,
                    'placeholder' => gT("None"),
                    'unselectValue' => "",
                ],
                'selectOptions' => [
                    'placeholder' => gT("None"),
                    ],
                'current' => $this->get('otherSurveys', 'Survey', $surveyId, [])
            ],
            "attributeToUse" => [
                'type' => 'select',
                'label' => $this->translate('Attribute used for token creation.'),
                'help' => $this->translate('Each value create or update a different token.'),
                'options' => $aTokenAttributes,
                'htmlOptions' => [
                    'empty' => gT("None"),
                ],
                'current' => $this->get('attributeToUse', 'Survey', $surveyId, "")
            ],
            "valuesToSet" => [
                'type' => 'text',
                "htmlOptions" => [
                    "rows" => 1,
                ],
                'label' => $this->translate('Values to be set.'),
                'help' => "",
                'current' => $this->get('valuesToSet', 'Survey', $surveyId, "")
            ],
            "valuesSeparator" => [
                'type' => 'text',
                "htmlOptions" => [
                    "rows" => 1,
                ],
                'label' => $this->translate('Separator of values.'),
                'help' => "",
                'current' => $this->get('valuesSeparator', 'Survey', $surveyId, ",")
            ]
        );
        /* Fix some help */
        if ($this->get('valuesToSet', 'Survey', $surveyId, "")) {
            $aSettings[$this->translate('Create tokens settings')]["valuesToSet"]["help"] = sprintf(
                $this->translate("Actual value: %s"),
                $this->ownGetHtmlExpression($this->get('valuesToSet', 'Survey', $surveyId, ""), $surveyId)
            );
        }

        $aSettings[$this->translate('Other value to create or use in relation')] = $this->ownGetOtherTokenSettings($surveyId);
        $aSettings[$this->translate('Value to use when search older tokens')] = array(
            "attributesDuplicateExclude" => [
                'type' => 'select',
                'label' => $this->translate('Attribute excluded from the duplicate search.'),
                'help' => $this->translate('When create the new token, exclude this attribute when searching for duplicates.'),
                'options' => $aTokenAttributes,
                'multiple' => true,
                'htmlOptions' => [
                    'empty' => gT("None"),
                    'multiple' => true,
                ],
                'current' => $this->get('attributesDuplicateExclude', 'Survey', $surveyId, "")
            ],
        );
        $aData = [];
        $aData["pluginClass"] = get_class($this);
        $aData["surveyId"] = $surveyId;
        $aData["title"] = $this->translate("Create tokens with this survey responses in other surveys");
        $aData["warningString"] = null;
        $aData["aSettings"] = $aSettings;
        $content = $this->renderPartial("settings", $aData, true);
        return $content;
    }
    /**
     * get the token attributes array
     * @para int $iSurveyId
     * @return array
     */
    private function ownGetTokensAttributes($iSurveyId)
    {
        $aTokenAttributes = [];
        if (empty(Survey::model()->findByPk($iSurveyId))) {
            return;
        }
        if (tableExists("{{tokens_{$iSurveyId}}}")) {
            $aRealTokenAttributes = array_keys(
                Yii::app()->db->schema->getTable("{{tokens_{$iSurveyId}}}")
                    ->columns
            );
            $aRealTokenAttributes = array_combine(
                $aRealTokenAttributes,
                $aRealTokenAttributes
            );
            $aTokenAttributes = array_filter(
                Token::model($iSurveyId)->attributeLabels()
            );
            $aTokenAttributes = array_replace(
                $aRealTokenAttributes,
                $aTokenAttributes
            );
        }
        if (!tableExists("{{tokens_{$iSurveyId}}}")) {
            $aRealTokenAttributes = [
                "firstname" => gT("First name"),
                "lastname" => gT("Last name"),
                "email" => gT("Email"),
            ];
            $aUserTokenAttributes = array_filter(
                Survey::model()
                    ->findByPk($iSurveyId)
                    ->getTokenAttributes()
            );

            $aUserTokenAttributes = array_map(function ($aTokenAttribute) {
                return $aTokenAttribute["description"];
            }, $aUserTokenAttributes);
            $aTokenAttributes = array_merge(
                $aRealTokenAttributes,
                $aUserTokenAttributes
            );
        }
        $aTokenAttributes = array_diff_key($aTokenAttributes, [
            "tid" => "tid",
            "partcipant" => "partcipant",
            "participant" => "participant",
            "participant_id" => "participant_id",
            "partcipant_id" => "partcipant_id",
            "emailstatus" => "emailstatus",
            "token" => "token",
            "language" => "language",
            "blacklisted" => "blacklisted",
            "sent" => "sent",
            "remindersent" => "remindersent",
            "remindercount" => "remindercount",
            "completed" => "completed",
            "usesleft" => "usesleft",
            "validfrom" => "validfrom",
            "validuntil" => "validuntil",
            "mpid" => "mpid",
        ]);
        return $aTokenAttributes;
    }

    /**
     * Get the complete HTML from a string with Expression for admin
     * @param string $sExpression : the string to parse
     * @param array $aReplacement : optionnal array of replacemement
     * @param boolean $forceEm : force EM or not
     *
     * @author Denis Chenu
     * @version 1.0
     */
    private function ownGetHtmlExpression(
        $sExpression,
        $iSurveyId,
        $aReplacement = [],
        $forceEm = false
    ) {
        $LEM = LimeExpressionManager::singleton();
        $aReData = [];
        if ($iSurveyId) {
            $LEM::StartSurvey($iSurveyId, "survey", [
                "hyperlinkSyntaxHighlighting" => true,
            ]); // replace QCODE
        }
        if ($forceEm) {
            $sExpression = "{" . $sExpression . "}";
        } else {
            $oFilter = new CHtmlPurifier();
            $sExpression = $oFilter->purify(
                viewHelper::filterScript($sExpression)
            );
        }
        LimeExpressionManager::ProcessString($sExpression);
        return $LEM::GetLastPrettyPrintExpression();
    }

    /**
     * Return a translated string
     * @param string
     * @return string
     */
    private function translate($string, $escapeMode = 'unescaped', $language = null)
    {
        if (method_exists($this, "gT")) {
            return $this->gT($string, $escapeMode, $language);
        }
        return $string;
    }

    /**
     * Return the survey list
     * @param string
     * @return string
     */
    private function ownGetSurveyListData()
    {
        $surveyListData = array();
        $aSurveys = Survey::model()
            ->permission(App()->user->getId())
            ->findAll([
                'select' => 'sid,language'
            ]);
        foreach ($aSurveys as $survey) {
            $surveyListData[$survey->sid] = $survey->getLocalizedTitle();
        }
        return $surveyListData;
    }

    /**
     * Get the survey settings
     * @param int $iSurveyId
     * @return array[]
     */
    private function ownGetOtherTokenSettings($iSurveyId)
    {
        $aTokenAttributes = $this->ownGetTokensAttributes($iSurveyId);
        $aSettings = [];
        if (!empty($aTokenAttributes)) {
            $oSurvey = Survey::model()->findByPk($iSurveyId);
            if (!tableExists("{{tokens_{$iSurveyId}}}")) {
                $aSettings["warningNoToken"] = [
                    "type" => "info",
                    "htmlOptions" => ["class" => "alert alert-warning"],
                    "content" => $this->translate(
                        "Participant is not currently activated on this survey. This settings are used only for survey with participant."
                    ),
                ];
            }
            foreach ($aTokenAttributes as $attribute => $description) {
                $description = trim($description) == "" ? $attribute : $description;
                $default = $this->get("other_attributes_{$attribute}", null, null, "");
                $aSettings["other_attributes_{$attribute}"] = [
                    "type" => "text",
                    "label" =>
                        "<strong>" .
                        $description .
                        "</strong> <small>" .
                        $this->translate("with") .
                        "</small>",
                    "current" => $this->get("other_attributes_{$attribute}", "Survey", $iSurveyId, ""),
                    "htmlOptions" => [
                        "rows" => 1,
                    ],
                ];
                $actualValue = $this->get(
                    "other_attributes_{$attribute}",
                    "Survey",
                    $iSurveyId,
                    ""
                );
                if ($actualValue) {
                    if (trim($actualValue) == "") {
                        $aSettings["other_attributes_{$attribute}"][
                            "help"
                        ] = $this->translate("Not updated");
                    } else {
                        $aSettings["other_attributes_{$attribute}"]["help"] = sprintf(
                            $this->translate("Actual value: %s"),
                            $this->ownGetHtmlExpression($actualValue, $iSurveyId)
                        );
                    }
                } elseif (!empty($default)) {
                    $aSettings["other_attributes_{$attribute}"]["help"] = sprintf(
                        $this->translate("Actual value (by default): %s"),
                        $this->ownGetHtmlExpression($default, $iSurveyId)
                    );
                }
            }
        }
        return $aSettings;
    }
}
